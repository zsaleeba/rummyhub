// Server-side part of the Go websocket sample.
//
package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"time"

	"gitlab.com/zsaleeba/rummyhub/rhserv"
	"golang.org/x/net/trace"
	"golang.org/x/net/websocket"
)

var (
	port   = flag.Int("port", 8080, "Rummyhub server port")
	world  *rhserv.World
	config *rhserv.Config
)

// Reasons why the turn changed.
const (
	TurnChangeCompleted = 0
	TurnChangeReset     = 1
	TurnChangeDrawn     = 2
)

// RummyMessage is used to parse incoming JSON messages, get the message
// type and message contents.
type RummyMessage struct {
	MsgType string           `json:"msgType"`
	Message *json.RawMessage `json:"msg"`
}

// RummyRequest is like a RummyMessage but with all the Player, Room and
// Game looked up and validated.
type RummyRequest struct {
	ws             *websocket.Conn
	writeMutex     *sync.Mutex
	tr             *trace.Trace
	msgType        string
	isValidSession bool
	player         *rhserv.Player
	game           *rhserv.Game
	msg            *json.RawMessage
}

// RummyResponse contains all other forms of response.
type RummyResponse struct {
	MsgType string          `json:"msgType"`
	ErrCode int             `json:"errCode"`
	ErrStr  string          `json:"errStr"`
	Message json.RawMessage `json:"msg"`
}

// RummyGameStateResponse sends the complete game state.
type RummyGameStateResponse struct {
	GameCode         uint              `json:"gameCode"`
	InProgress       bool              `json:"inProgress"`
	PlayersTurn      uint64            `json:"playersTurn"`
	TableModified    bool              `json:"boardModified"`
	TableValid       bool              `json:"tableValid"`
	GameWon          bool              `json:"gameWon"`
	GameWonBy        uint64            `json:"gameWonBy"`
	Players          []RummyPlayerInfo `json:"players"`
	Table            *rhserv.TileArea  `json:"table"`
	TableAtTurnStart *rhserv.TileArea  `json:"tableAtTurnStart"`
	Tray             *rhserv.TileArea  `json:"tray"`
}

// RummyJoinGameParams is the parameters for a newGame request.
type RummyJoinGameParams struct {
	GameCode   uint   `json:"gameCode"`
	PlayerName string `json:"playerName"`
}

// RummyJoinGameResponse is a response to a newGame message.
type RummyJoinGameResponse struct {
	PlayerID   uint64            `json:"playerId"`
	GameID     uint64            `json:"gameId"`
	GameLeader bool              `json:"gameLeader"`
	Players    []RummyPlayerInfo `json:"players"`
}

// RummyPlayerInfo describes a player as seen by other players.
type RummyPlayerInfo struct {
	PlayerID  uint64 `json:"playerId"`
	Name      string `json:"name"`
	Score     int    `json:"score"`
	Observing bool   `json:"observing"`
}

// RummyPlayerJoinedResponse is sent when a new player joins a game.
type RummyPlayerJoinedResponse struct {
	PlayerID  uint64 `json:"playerId"`
	Name      string `json:"name"`
	Score     int    `json:"score"`
	Observing bool   `json:"observing"`
}

// RummyNewGameResponse is a response to a newGame message.
type RummyNewGameResponse struct {
	GameCode uint `json:"gameCode"`
}

// Areas a tile can go from or to.
const (
	AreaBag int = iota
	AreaTable
	AreaPlayerTray
	AreaOpponentTray
	AreaDestroyed
)

// TileLoc identifies where a tile is.
type TileLoc struct {
	Area int `json:"area"`
	Col  int `json:"col"`
	Row  int `json:"row"`
}

// MoveSingleTile describes a single tile's movement.
// It contains the numeric code of the tile which was drawn.
// 0 if none left.
type MoveSingleTile struct {
	Tile int     `json:"tile"`
	From TileLoc `json:"from"`
	To   TileLoc `json:"to"`
}

// MoveTileRequest requests to move a tile from one location
// to another within a Game. It will most likely result in a
// MoveTilesResponse.
type MoveTileRequest struct {
	Move MoveSingleTile `json:"move"`
}

// MoveTilesResponse is the response to a drawTile message.
// It contains the numeric code of the tile which was drawn.
// 0 if none left.
type MoveTilesResponse struct {
	Moves         []MoveSingleTile `json:"moves"`
	TableModified bool             `json:"boardModified"`
	TableValid    bool             `json:"tableValid"`
	GameWon       bool             `json:"gameWon"`
	GameWonBy     uint64           `json:"gameWonBy"`
}

// TurnChangeResponse is sent when the turn changes.
type TurnChangeResponse struct {
	FromPlayerID uint64 `json:"fromPlayerId"`
	ToPlayerID   uint64 `json:"toPlayerId"`
	Reason       int    `json:"reason"`
}

// RummyGameWonResponse notifies players of a game being won.
type RummyGameWonResponse struct {
	GameWon   bool   `json:"gameWon"`
	GameWonBy uint64 `json:"gameWonBy"`
}

// checkGame checks for a valid game.
func checkGame(req *RummyRequest) *rhserv.RhErr {
	if req.game == nil {
		return &rhserv.RhErr{Error: errors.New("invalid game"), Message: "Not a valid game", Code: rhserv.ErrorCodeGameNotFound}
	}

	return nil
}

// checkGameAndPlayer checks for a valid game and player.
func checkGameAndPlayer(req *RummyRequest) *rhserv.RhErr {
	if err := checkGame(req); err != nil {
		return err
	}

	if req.player == nil {
		return &rhserv.RhErr{Error: errors.New("invalid player"), Message: "Not a valid player", Code: rhserv.ErrorCodeInvalidParameters}
	}

	return nil
}

// checkGameAndPlayerTurn checks for a valid game and player and make's sure it's
// the player's turn.
func checkGameAndPlayerTurn(req *RummyRequest) *rhserv.RhErr {
	if err := checkGameAndPlayer(req); err != nil {
		return err
	}

	if req.player.ID != req.game.PlayersTurn {
		return &rhserv.RhErr{Error: errors.New("not player's turn"), Message: "It's not your turn", Code: rhserv.ErrorCodeNotPlayerTurn}
	}

	return nil
}

// handleMoveTileMessage handles a request to move a tile around.
func handleMoveTileMessage(req *RummyRequest) error {
	// Get the parameters.
	var params MoveTileRequest
	if err := json.Unmarshal(*req.msg, &params); err != nil {
		log.Println("move tile invalid parameters", err, ":", string(*req.msg))
		return sendRummyResponse(req, "moveTileResp", rhserv.ErrorCodeInvalidParameters, "invalid parameters", struct{}{})
	}

	// Check for valid game and player, and that it's our turn.
	rhErr := checkGameAndPlayer(req)
	if rhErr != nil {
		log.Println(rhErr.Error)
		return sendRummyResponse(req, "moveTileResp", rhErr.Code, rhErr.Message, struct{}{})
	}

	// Check if we can make the move.
	var tileID = rhserv.Tile(params.Move.Tile)
	var fromAreaID = params.Move.From.Area
	var fromArea *rhserv.TileArea = nil
	var fromCol = params.Move.From.Col
	var fromRow = params.Move.From.Row

	if fromAreaID == AreaPlayerTray {
		fromArea = req.player.Tray
	} else if fromAreaID == AreaTable {
		fromArea = req.game.Table
	}

	var toAreaID = params.Move.To.Area
	var toArea *rhserv.TileArea = nil
	var toCol = params.Move.To.Col
	var toRow = params.Move.To.Row

	if toAreaID == AreaPlayerTray {
		toArea = req.player.Tray
	} else if toAreaID == AreaTable {
		toArea = req.game.Table
	}

	// We can move a tile around our tray if it's not our turn or if it is our turn
	// we can move most ways.
	if req.player.ID != req.game.PlayersTurn && (toAreaID != AreaPlayerTray || fromAreaID != AreaPlayerTray) {
		log.Println("not player's turn")
		return sendRummyResponse(req, "moveTileResp", rhserv.ErrorCodeNotPlayerTurn, "It's not your turn", struct{}{})
	}

	if fromArea == nil ||
		fromCol < 0 || fromCol >= fromArea.Cols ||
		fromRow < 0 || fromRow >= fromArea.Rows ||
		fromArea.Tiles[fromRow][fromCol] != tileID {
		log.Println("move tile: no such tile:", string(*req.msg))
		return sendRummyResponse(req, "moveTileResp", rhserv.ErrorCodeInvalidTile, "invalid tile source", struct{}{})
	}

	if toArea == nil ||
		toCol < 0 || toCol >= toArea.Cols ||
		toRow < 0 || toRow >= toArea.Rows ||
		toArea.Tiles[toRow][toCol] != 0 {
		log.Println("move tile: no such tile:", string(*req.msg))
		return sendRummyResponse(req, "moveTileResp", rhserv.ErrorCodeInvalidTile, "invalid tile dest", struct{}{})
	}

	// Are they trying to move a tile that isn't theirs from the table to their hand?
	if fromAreaID == AreaTable && toAreaID == AreaPlayerTray &&
		req.game.TileWasFromTable(tileID) {
		log.Println("move tile: this isn't your tile to take:", string(*req.msg))
		return sendRummyResponse(req, "moveTileResp", rhserv.ErrorCodeNotYourTile, "not your tile to take", struct{}{})
	}

	// Move it.
	log.Println("move tile from", fromCol, fromRow, fromAreaID, "to", toCol, toRow, toAreaID)
	toArea.Tiles[toRow][toCol] = fromArea.Tiles[fromRow][fromCol]
	fromArea.Tiles[fromRow][fromCol] = 0

	// Check if the table is modified and if it's valid.
	req.game.UpdateStatus()

	// Save it.
	if fromAreaID == AreaPlayerTray || toAreaID == AreaPlayerTray {
		world.PutPlayer(req.player)
	}

	if fromAreaID == AreaTable || toAreaID == AreaTable {
		world.PutGame(req.game)
	}

	// Respond to the player.
	moveResponse := MoveTilesResponse{[]MoveSingleTile{params.Move}, req.game.TableModified, req.game.TableValid, req.game.GameWon, req.game.GameWonBy}
	if err := sendRummyResponse(req, "moveTileResp", rhserv.ErrorCodeNone, "", moveResponse); err != nil {
		return err
	}

	// Tell all the other players, but only if it's not private to our own tray.
	if fromAreaID != AreaPlayerTray || toAreaID != AreaPlayerTray {
		othersFrom := params.Move.From
		if fromAreaID == AreaPlayerTray {
			othersFrom.Area = AreaOpponentTray
			othersFrom.Col = 0
			othersFrom.Row = 0
		}

		othersTo := params.Move.To
		if toAreaID == AreaPlayerTray {
			othersTo.Area = AreaOpponentTray
			othersTo.Col = 0
			othersTo.Row = 0
		}

		othersMove := MoveSingleTile{params.Move.Tile, othersFrom, othersTo}
		othersNotify := MoveTilesResponse{[]MoveSingleTile{othersMove}, req.game.TableModified, req.game.TableValid, req.game.GameWon, req.game.GameWonBy}

		if err := sendToOthers(req, req.player.ID, "moveTiles", rhserv.ErrorCodeNone, "", othersNotify); err != nil {
			return err
		}
	}

	return nil
}

// handleDrawTileMessage handles the message e arriving on connection ws
// from the client. It will most likely result in a MoveTilesResponse.
func handleDrawTileMessage(req *RummyRequest) error {
	// Check for valid game and player, and that it's our turn.
	rhErr := checkGameAndPlayerTurn(req)
	if rhErr != nil {
		log.Println(rhErr.Error)
		return sendRummyResponse(req, "drawTileResp", rhErr.Code, rhErr.Message, struct{}{})
	}

	// Has the board been modified?
	if req.game.TableModified {
		return sendRummyResponse(req, "drawTileResp", rhserv.ErrorCodeInvalidCommand, "You can't draw - you already made a move", struct{}{})
	}

	// Get a tile.
	tile := req.game.GetBag().DrawTile()

	// Find a place to put the tile in the player's tray.
	col, row, err := req.player.Tray.FindEmptySpace()
	if err != nil {
		return err
	}

	// Put it in the tray.
	if err := req.player.Tray.PutTile(col, row, tile); err != nil {
		return err
	}

	// Change the turn to the next player.
	nextPlayer := nextPlayersTurn(req.game)
	req.game.StartOfTurn()
	nextPlayer.StartOfTurn()

	// Save the changes.
	req.game.UpdateStatus()
	world.PutPlayer(req.player)
	world.PutPlayer(nextPlayer)
	world.PutGame(req.game)

	// Respond to the player.
	content := &TurnChangeResponse{FromPlayerID: req.player.ID, ToPlayerID: req.game.PlayersTurn, Reason: TurnChangeDrawn}
	if err := sendRummyResponse(req, "drawTileResp", rhserv.ErrorCodeNone, "", content); err != nil {
		return err
	}

	// Tell the player about the new tiles.
	singleMove := MoveSingleTile{int(tile), TileLoc{AreaBag, 0, 0}, TileLoc{AreaPlayerTray, col, row}}
	msg := MoveTilesResponse{[]MoveSingleTile{singleMove}, req.game.TableModified, req.game.TableValid, req.game.GameWon, req.game.GameWonBy}
	err = sendRummyResponse(req, "moveTiles", 0, "", msg)
	if err != nil {
		return fmt.Errorf("Can't send: %s", err.Error())
	}

	// Tell everyone else about the change of turn.
	if err := sendToOthers(req, req.player.ID, "turnChange", rhserv.ErrorCodeNone, "", content); err != nil {
		return err
	}

	return nil
}

// handleTurnDoneMessage
func handleTurnDoneMessage(req *RummyRequest) error {
	// Check for valid game and player, and that it's our turn.
	rhErr := checkGameAndPlayerTurn(req)
	if rhErr != nil {
		log.Println(rhErr.Error)
		return sendRummyResponse(req, "turnDoneResp", rhErr.Code, rhErr.Message, struct{}{})
	}

	// Has the board been modified?
	if !req.game.TableModified {
		return sendRummyResponse(req, "turnDoneResp", rhserv.ErrorCodeInvalidCommand, "You can't submit your move - you haven't made a move", struct{}{})
	}

	// Is the board in a valid state?
	if !req.game.TableValid {
		return sendRummyResponse(req, "turnDoneResp", rhserv.ErrorCodeTableInvalid, "You can't submit your move - the table isn't valid", struct{}{})
	}

	// Did this player just win the game?
	if req.player.Tray.IsEmpty() {
		req.game.GameWon = true
		req.game.GameWonBy = req.player.ID
	}

	// Change the turn to the next player.
	nextPlayer := req.player
	if !req.game.GameWon {
		nextPlayer = nextPlayersTurn(req.game)
		nextPlayer.StartOfTurn()
	}

	// Copy the the table to the "turn start" state.
	req.game.StartOfTurn()

	// Save the changes.
	req.game.UpdateStatus()
	world.PutGame(req.game)
	world.PutPlayer(req.player)
	if nextPlayer != req.player {
		world.PutPlayer(nextPlayer)
	}

	// Tell everyone.
	content := &TurnChangeResponse{FromPlayerID: req.player.ID, ToPlayerID: req.game.PlayersTurn, Reason: TurnChangeCompleted}

	if err := sendRummyResponse(req, "turnDoneResp", rhserv.ErrorCodeNone, "", content); err != nil {
		return err
	}

	if !req.game.GameWon {
		// Tell others about the change of turn.
		if err := sendToOthers(req, req.player.ID, "turnChange", rhserv.ErrorCodeNone, "", content); err != nil {
			return err
		}
	} else {
		// Tell everyone that this player won.
		gameWonMsg := &RummyGameWonResponse{GameWon: true, GameWonBy: req.player.ID}
		if err := sendToOthers(req, 0, "gameWon", rhserv.ErrorCodeNone, "", gameWonMsg); err != nil {
			return err
		}
	}
	return nil
}

// handleResetTurnMessage handles a request to reset the turn.
func handleResetTurnMessage(req *RummyRequest) error {
	// Check for valid game and player, and that it's our turn.
	rhErr := checkGameAndPlayerTurn(req)
	if rhErr != nil {
		log.Println(rhErr.Error)
		return sendRummyResponse(req, "resetTurnResp", rhErr.Code, rhErr.Message, struct{}{})
	}

	// Has the board been modified?
	if !req.game.TableModified {
		return sendRummyResponse(req, "resetTurnResp", rhserv.ErrorCodeInvalidCommand, "You can't reset your move - you haven't made a move", struct{}{})
	}

	// Revert the table state.
	req.game.RevertTurn()
	req.player.RevertTurn()

	// playerMovedTiles := []MoveSingleTile{}
	// publicMovedTiles := []MoveSingleTile{}

	// for _, rt := range removedTiles {
	// 	// Find a place to put the tile in the player's tray.
	// 	toCol, toRow, err := req.player.Tray.FindEmptySpace()
	// 	if err != nil {
	// 		return err
	// 	}

	// 	// Put it in the tray.
	// 	if err = req.player.Tray.PutTile(toCol, toRow, rt.T); err != nil {
	// 		return err
	// 	}

	// 	// Keep track of what moved where.
	// 	fromLoc := TileLoc{AreaTable, rt.Col, rt.Row}
	// 	playerMovedTiles = append(playerMovedTiles, MoveSingleTile{int(rt.T), fromLoc, TileLoc{AreaPlayerTray, toCol, toRow}})
	// 	publicMovedTiles = append(publicMovedTiles, MoveSingleTile{int(rt.T), fromLoc, TileLoc{AreaOpponentTray, 0, 0}})
	// }

	// fmt.Println("playerMovedTiles before adding", playerMovedTiles)
	// fmt.Println("publicMovedTiles before adding", publicMovedTiles)

	// Draw a tile. Or more if you add more to this slice.
	addTiles := []rhserv.Tile{req.game.GetBag().DrawTile()}

	// Put these drawn tiles in the player's tray.
	for _, t := range addTiles {
		// Find a place to put the tile in the player's tray.
		col, row, err := req.player.Tray.FindEmptySpace()
		if err != nil {
			return err
		}

		// Put it in the tray.
		if err = req.player.Tray.PutTile(col, row, t); err != nil {
			return err
		}

		// Keep track of what moved where.
		// fromLoc := TileLoc{AreaBag, 0, 0}
		// playerMovedTiles = append(playerMovedTiles, MoveSingleTile{int(t), fromLoc, TileLoc{AreaPlayerTray, col, row}})
		// publicMovedTiles = append(publicMovedTiles, MoveSingleTile{int(t), fromLoc, TileLoc{AreaOpponentTray, 0, 0}})
	}

	// Change the turn to the next player.
	nextPlayer := nextPlayersTurn(req.game)
	req.game.UpdateStatus()

	// Save the changes.
	world.PutPlayer(req.player)
	world.PutPlayer(nextPlayer)
	world.PutGame(req.game)

	// Update everyone with the entire game state.
	updateAllGameState(req, req.game, true)

	// // Tell the player.
	// playerMsg := MoveTilesResponse{playerMovedTiles, req.game.TableModified, req.game.TableValid, req.game.GameWon, req.game.GameWonBy}

	// if err := sendRummyResponse(req, "moveTiles", rhserv.ErrorCodeNone, "", playerMsg); err != nil {
	// 	return fmt.Errorf("Can't send: %s", err.Error())
	// }

	// turnChangeMsg := &TurnChangeResponse{FromPlayerID: req.player.ID, ToPlayerID: req.game.PlayersTurn, Reason: TurnChangeReset}
	// if err := sendRummyResponse(req, "resetTurnResp", rhserv.ErrorCodeNone, "", turnChangeMsg); err != nil {
	// 	return err
	// }

	// // Tell everyone else.
	// publicMsg := MoveTilesResponse{playerMovedTiles, req.game.TableModified, req.game.TableValid, req.game.GameWon, req.game.GameWonBy}

	// if err := sendToOthers(req, req.player.ID, "moveTiles", rhserv.ErrorCodeNone, "", publicMsg); err != nil {
	// 	return err
	// }

	// if err := sendToOthers(req, req.player.ID, "turnChange", rhserv.ErrorCodeNone, "", turnChangeMsg); err != nil {
	// 	return err
	// }

	return nil
}

// nextPlayersTurn goes to the next player and skips observers.
func nextPlayersTurn(g *rhserv.Game) *rhserv.Player {
	// Get the player list and eliminate observers.
	players := g.GetPlayers()
	for i := len(players) - 1; i >= 0; i-- {
		if players[i].Observing {
			players = append(players[0:i], players[i+1:]...)
		}
	}

	// Find the player in the list.
	for i, p := range players {
		if p.ID == g.PlayersTurn {
			if i < len(players)-1 {
				g.PlayersTurn = players[i+1].ID
				return players[i+1]
			} else {
				g.PlayersTurn = players[0].ID
				return players[0]
			}
		}
	}

	// Didn't find it? Just make it the first player.
	if len(players) > 0 {
		g.PlayersTurn = players[0].ID
	} else {
		// No players, no game.
		g.PlayersTurn = 0
		g.InProgress = false
	}

	return nil
}

// handleStartGameMessage
func handleStartGameMessage(req *RummyRequest) error {
	// Check for valid game.
	rhErr := checkGame(req)
	if rhErr != nil {
		log.Println(rhErr.Error)
		return sendRummyResponse(req, "startGameResp", rhErr.Code, rhErr.Message, struct{}{})
	}

	if len(req.game.PlayerIDs) < 1 || req.player.ID != req.game.PlayerIDs[0] {
		return errors.New("not allowed to start a game")
	}

	log.Println("start game", req.game.ID, req.player.ID)
	err := req.game.StartGame()
	if err != nil {
		log.Println("startGame", err.Error())
		return sendRummyResponse(req, "startGameResp", rhserv.ErrorCodeCantStart, err.Error(), struct{}{})
	}

	world.PutGameAndPlayers(req.game)

	if err := updateAllGameState(req, req.game, true); err != nil {
		return err
	}

	return sendToOthers(req, 0, "startGameResp", rhserv.ErrorCodeNone, "", struct{}{})
}

// handleResignGameMessage
func handleResignGameMessage(req *RummyRequest) error {
	return nil
}

// handleGetGameStateMessage
func handleGetGameStateMessage(req *RummyRequest) error {
	// Check for valid game.
	rhErr := checkGame(req)
	if rhErr != nil {
		log.Println(rhErr.Error)
		return sendRummyResponse(req, "gameState", rhErr.Code, rhErr.Message, struct{}{})
	}

	playerInfo := makePlayerInfo(req.game)
	gameState := RummyGameStateResponse{req.game.GameCode, req.game.InProgress, req.game.PlayersTurn, req.game.TableModified, req.game.TableValid, req.game.GameWon, req.game.GameWonBy, *playerInfo, req.game.Table, req.game.TableAtTurnStart, req.player.Tray}
	if err := sendRummyResponse(req, "gameState", rhserv.ErrorCodeNone, "", gameState); err != nil {
		return err
	}

	return nil
}

// handleJoinGameMessage
func handleJoinGameMessage(req *RummyRequest) error {
	// Get the parameters.
	var params RummyJoinGameParams
	if err := json.Unmarshal(*req.msg, &params); err != nil {
		log.Println("join game invalid parameters", err, ":", string(*req.msg))
		return sendRummyResponse(req, "joinGameResp", rhserv.ErrorCodeInvalidParameters, "invalid parameters", struct{}{})
	}

	// Get the game ID from the game code.
	gameID := world.GetGameIDByGameCode(params.GameCode)
	if gameID == 0 {
		return sendRummyResponse(req, "joinGameResp", rhserv.ErrorCodeGameNotFound, "game not found", struct{}{})
	}

	// Get the game.
	g := world.GetGame(gameID)
	if g == nil {
		return sendRummyResponse(req, "joinGameResp", rhserv.ErrorCodeGameNotFound, "game not found", struct{}{})
	}

	var gameLeader = len(g.PlayerIDs) == 0

	// Get the player by name if they're already in.
	p := g.GetPlayerByName(params.PlayerName)
	if p == nil {
		// There's no player with that name - add them.
		p = rhserv.NewPlayer(params.PlayerName)
	}

	if p.GameID != g.ID {
		// Add them to the game.
		p.JoinGame(g, !g.InProgress)
		g.AddPlayer(p)
	}

	world.PutPlayer(p)
	world.PutGame(g)

	log.Println("join game", params.GameCode, params.PlayerName, g.ID, p.ID)

	// Keep note of the logged-in player and game for future requests.
	req.player = p
	req.game = g
	p.Socket = req.ws
	p.WriteMutex = req.writeMutex

	// Tell all the other players they've joined.
	sendToOthers(req, p.ID, "playerJoinedResp", rhserv.ErrorCodeNone, "", RummyPlayerJoinedResponse{p.ID, p.Name, p.Score, p.Observing})

	// Send a response to them.
	return sendRummyResponse(req, "joinGameResp", rhserv.ErrorCodeNone, "", RummyJoinGameResponse{p.ID, g.ID, gameLeader, *makePlayerInfo(g)})
}

// makePlayerInfo creates a RummyPlayerInfo from a Game.
func makePlayerInfo(g *rhserv.Game) *[]RummyPlayerInfo {
	players := g.GetPlayers()
	playerInfo := make([]RummyPlayerInfo, len(players))
	for i, pl := range players {
		playerInfo[i] = RummyPlayerInfo{pl.ID, pl.Name, pl.Score, pl.Observing}
	}

	return &playerInfo
}

// handleNewGameMessage
func handleNewGameMessage(req *RummyRequest) error {
	// Create a new game.
	g := world.NewUniqueGame()

	log.Printf("Created new game %d", g.GameCode)

	// Tell the client the game code.
	return sendRummyResponse(req, "newGameResp", rhserv.ErrorCodeNone, "", RummyNewGameResponse{g.GameCode})
}

// handleInvalidCommand responds to an invalid command.
func handleInvalidCommand(req *RummyRequest) error {
	log.Println("invalid command")
	return sendRummyResponse(req, req.msgType, rhserv.ErrorCodeInvalidCommand, "invalid command", struct{}{})
}

// sendRummyResponse sends a JSON response to a command.
func sendRummyResponse(req *RummyRequest, msgType string, errCode int, errStr string, respObj interface{}) error {
	// Marshal the object.
	msgData, _ := json.Marshal(&respObj)
	msgRaw := json.RawMessage(string(msgData))
	resp := RummyResponse{msgType, errCode, errStr, msgRaw}

	// Send it.
	req.writeMutex.Lock()
	err := websocket.JSON.Send(req.ws, &resp)
	req.writeMutex.Unlock()
	if err != nil {
		return fmt.Errorf("can't send: %s", err.Error())
	}
	return nil
}

// sendToOthers sends a JSON response to the other players, but not this one.
func sendToOthers(req *RummyRequest, exceptID uint64, msgType string, errCode int, errStr string, respObj interface{}) error {
	// Only do anything if they're logged into a game.
	if req.game == nil || req.player == nil {
		return nil
	}

	// Marshal the object.
	msgData, _ := json.Marshal(&respObj)
	msgRaw := json.RawMessage(string(msgData))
	resp := RummyResponse{msgType, 0, "", msgRaw}

	// Send it to all of them.
	players := req.game.GetPlayers()
	for _, to := range players {
		if to.ID != exceptID && to.Socket != nil {
			to.WriteMutex.Lock()
			err := websocket.JSON.Send(to.Socket, &resp)
			to.WriteMutex.Unlock()
			if err != nil {
				// Mark them as disconnected.
				to.Socket = nil
				return fmt.Errorf("can't send: %s", err.Error())
			}
		}
	}

	return nil
}

// sendToPlayer sends a JSON response to a specific player.
func sendToPlayer(player *rhserv.Player, msgType string, errCode int, errStr string, respObj interface{}) error {
	// Only do anything if they exist and are logged in.
	if player == nil || player.Socket == nil {
		return nil
	}

	// Marshal the object.
	msgData, _ := json.Marshal(&respObj)
	msgRaw := json.RawMessage(string(msgData))
	resp := RummyResponse{msgType, 0, "", msgRaw}

	// Send it to them.
	player.WriteMutex.Lock()
	err := websocket.JSON.Send(player.Socket, &resp)
	player.WriteMutex.Unlock()
	if err != nil {
		// Mark them as disconnected.
		player.Socket = nil
		return fmt.Errorf("can't send: %s", err.Error())
	}

	return nil
}

// updateAllGameState sends an update to all players containing the complete
// game state.
func updateAllGameState(req *RummyRequest, g *rhserv.Game, meToo bool) error {
	playerInfo := makePlayerInfo(g)
	players := g.GetPlayers()

	for _, to := range players {
		if meToo || to.ID != req.player.ID {
			gameState := RummyGameStateResponse{g.GameCode, g.InProgress, g.PlayersTurn, req.game.TableModified, req.game.TableValid, req.game.GameWon, req.game.GameWonBy, *playerInfo, g.Table, g.TableAtTurnStart, to.Tray}
			if err := sendToPlayer(to, "gameState", rhserv.ErrorCodeNone, "", gameState); err != nil {
				return err
			}
		}
	}

	return nil
}

// websocketRummyhubConnection handles a single websocket - ws.
func websocketRummyhubConnection(ws *websocket.Conn) {
	log.Printf("Client connected from %s", ws.RemoteAddr())

	// Make a request. We'll re-use this for all our messages.
	var req RummyRequest
	req.ws = ws
	req.writeMutex = &sync.Mutex{}

	for {
		// Log the request with net.Trace
		tr := trace.New("websocket.Receive", "receive")
		defer tr.Finish()

		// Decode the message.
		var rMsg RummyMessage
		err := websocket.JSON.Receive(ws, &rMsg)
		if err != nil {
			log.Printf("Receive failed: %s; closing connection...", err.Error())
			tr.LazyPrintf("Receive failed: %s; closing connection...", err.Error())
			if err = ws.Close(); err != nil {
				log.Println("Error closing connection:", err.Error())
			}
			break
		} else {
			tr.LazyPrintf("Got event %v\n", rMsg)

			// Try to validate the fields.
			req.tr = &tr
			req.msgType = rMsg.MsgType
			req.msg = rMsg.Message

			if req.game != nil && req.player != nil {
				log.Println("cmd:", req.msgType, req.game.ID, req.player.ID, req.player.Name)
			} else {
				log.Println("cmd:", req.msgType)
			}

			// Handle different command types.
			var err error = nil

			if req.msgType == "moveTile" {
				log.Println("moveTile")
				err = handleMoveTileMessage(&req)
			} else if req.msgType == "drawTile" {
				log.Println("drawTile")
				err = handleDrawTileMessage(&req)
			} else if req.msgType == "turnDone" {
				log.Println("turnDone")
				err = handleTurnDoneMessage(&req)
			} else if req.msgType == "resetTurn" {
				log.Println("resetTurn")
				err = handleResetTurnMessage(&req)
			} else if req.msgType == "startGame" {
				log.Println("startGame")
				err = handleStartGameMessage(&req)
			} else if req.msgType == "resignGame" {
				log.Println("resignGame")
				err = handleResignGameMessage(&req)
			} else if req.msgType == "getGameState" {
				log.Println("getGameState")
				err = handleGetGameStateMessage(&req)
			} else if req.msgType == "joinGame" {
				err = handleJoinGameMessage(&req)
			} else if req.msgType == "newGame" {
				err = handleNewGameMessage(&req)
			} else {
				err = handleInvalidCommand(&req)
			}

			if err != nil {
				log.Println(err.Error())
				break
			}
		}
	}
}

// Main program.
func main() {
	flag.Parse()

	rand.Seed(time.Now().UnixNano())

	// Set up websocket servers and static file server. In addition, we're using
	// net/trace for debugging - it will be available at /debug/requests.
	http.Handle("/rhserv", websocket.Handler(websocketRummyhubConnection))
	http.Handle("/", http.FileServer(http.Dir("www")))

	// Read the config file.
	config = rhserv.NewConfig()

	// Create the data store.
	world = rhserv.NewWorld(config)

	// Start serving.
	log.Printf("Server listening on port %d", *port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), nil))
}

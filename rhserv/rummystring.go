package rhserv

import (
	"fmt"
	"sort"
)

const (
	RummyStringTypeSequence = iota
	RummyStringTypeSet
	RummyStringTypeInvalid
)

// RummyString is a series of tiles which form a set or sequence by the rules
// of rummy.
type RummyString struct {
	Col   int    // Where on the table it starts.
	Row   int    // Where on the table it starts.
	Type  int    // What type of string: RummyStringSequence or RummyStringSet.
	Tiles []Tile // The Tiles in the string, in sorted order.
}

// NewRummyString creates a RummyString.
func NewRummyString(col int, row int, tiles []Tile) *RummyString {
	rs := new(RummyString)
	rs.Col = col
	rs.Row = row
	rs.Type = RummyStringTypeInvalid
	rs.Tiles = tiles

	if rs.IsRummySequence() {
		rs.Type = RummyStringTypeSequence
	} else if rs.IsRummySet() {
		rs.Type = RummyStringTypeSet
	}

	return rs
}

// Equals compares two RummyStrings. It ignores their placement on the
// TileArea.
func (rs *RummyString) Equals(rs2 *RummyString) bool {
	if rs.Type != rs2.Type || len(rs.Tiles) != len(rs2.Tiles) {
		return false
	}

	for i := range rs.Tiles {
		if rs.Tiles[i] != rs2.Tiles[i] {
			return false
		}
	}

	return true
}

// Canonicalise put a RummyString in a canonical order. This ensures that two sorted RummyStrings
// will be equivalent. Sets are sorted, sequences are not since they're already in a specific
// order.
func (rs *RummyString) Canonicalise() *RummyString {
	// Copy it.
	result := new(RummyString)
	result.Col = rs.Col
	result.Row = rs.Row
	result.Type = rs.Type
	result.Tiles = make([]Tile, len(rs.Tiles))

	for i := range rs.Tiles {
		result.Tiles[i] = rs.Tiles[i]
	}

	// Sort in order unless it's a sequence.
	if result.Type != RummyStringTypeSequence {
		sort.Sort(result)
	}

	return result
}

// StringLessThan compares two RummyStrings.
func (rs *RummyString) StringLessThan(rs2 *RummyString) bool {
	if rs.Type < rs2.Type {
		return true // Type is less.
	}

	if rs.Type > rs2.Type {
		return false // Type is greater.
	}

	// Loop the length of the shorter of the two strings.
	for i := 0; i < len(rs.Tiles) && i < len(rs2.Tiles); i++ {
		if rs.Tiles[i].Compare(rs2.Tiles[i]) == TileLess {
			return true // This tile is less.
		}
	}

	// Everything so far has been identical. What about string length?
	if len(rs.Tiles) < len(rs2.Tiles) {
		return true // Shorter.
	}

	// So the length of the second string is greater or equal to the length of the first string.
	return false
}

// IsRummySequence checks if a RummyString is a valid sequence.
func (rs *RummyString) IsRummySequence() bool {
	if len(rs.Tiles) < 3 {
		return false
	}

	// Find the suit.
	sequenceSuit := SuitNone
	startVal := 0
	for i, t := range rs.Tiles {
		thisSuit, thisVal := t.GetVisibleAttrs()
		if thisSuit != SuitWild && thisSuit != SuitNone {
			sequenceSuit = thisSuit
			startVal = thisVal - i
		}
	}

	// Is it a valid sequence?
	for i, t := range rs.Tiles {
		thisSuit, thisVal := t.GetVisibleAttrs()
		tileOk := thisSuit == SuitWild ||
			(thisSuit == sequenceSuit && thisVal == startVal+i)
		if !tileOk {
			return false
		}
	}

	return true
}

// IsRummySet checks if a RummyString is a valid rummy "set". ie. All face values are the same.
func (rs *RummyString) IsRummySet() bool {
	if len(rs.Tiles) < 3 || len(rs.Tiles) > 4 {
		return false
	}

	// Find the face value.
	setValue := 0
	for _, t := range rs.Tiles {
		thisSuit, thisVal := t.GetVisibleAttrs()
		if thisSuit != SuitWild && thisSuit != SuitNone {
			setValue = thisVal
		}
	}

	// Do they all have the same face value?
	for _, t := range rs.Tiles {
		thisSuit, thisVal := t.GetVisibleAttrs()
		if thisSuit != SuitWild && thisVal != setValue {
			return false
		}
	}

	return true
}

// Sorting interface to put the tiles in RummyStrings in sorted order.
func (rs *RummyString) Len() int      { return len(rs.Tiles) }
func (rs *RummyString) Swap(i, j int) { rs.Tiles[i], rs.Tiles[j] = rs.Tiles[j], rs.Tiles[i] }
func (rs *RummyString) Less(i, j int) bool {
	return rs.Tiles[i].Compare(rs.Tiles[j]) == TileLess
}

// Print displays a RummyString for debug purposes.
func (rs *RummyString) Print() {
	fmt.Print(rs.Col, ",", rs.Row, " ", string(rs.Type), ": ")
	for _, t := range rs.Tiles {
		t.Print()
		fmt.Print(" ")
	}
	fmt.Println()
}

package rhserv

import (
	"sync"
	"time"

	"golang.org/x/net/websocket"
)

// Size of a player tray.
const (
	PlayerTrayCols = 13
	PlayerTrayRows = 3
)

// Player represents a player within a Game.
type Player struct {
	ID              uint64
	SavedTime       time.Time
	Name            string
	GameID          uint64
	Score           int
	Tray            *TileArea
	TrayAtTurnStart *TileArea
	Playing         bool
	Observing       bool
	Game            *Game           `json:"-"`
	Socket          *websocket.Conn `json:"-"`
	WriteMutex      *sync.Mutex     `json:"-"`
}

// NewPlayer creates a new Player.
func NewPlayer(name string) *Player {
	p := new(Player)
	p.ID = MakeRandomID()
	p.Name = name
	p.Tray = NewTileArea(PlayerTrayCols, PlayerTrayRows)
	p.TrayAtTurnStart = NewTileArea(PlayerTrayCols, PlayerTrayRows)
	p.SavedTime = time.Now()
	p.WriteMutex = &sync.Mutex{}

	return p
}

// JoinGame joins a player to a given game.
func (p *Player) JoinGame(g *Game, playing bool) {
	p.GameID = g.ID
	p.Score = 0
	p.Tray = NewTileArea(PlayerTrayCols, PlayerTrayRows)
	p.TrayAtTurnStart = NewTileArea(PlayerTrayCols, PlayerTrayRows)
	p.Playing = playing
	p.Observing = !playing
	p.Game = g
}

// StartOfTurn is called at the start of each turn.
func (p *Player) StartOfTurn() {
	p.TrayAtTurnStart = p.Tray.Copy()
}

// RevertTurn reverts a turn's changes.
func (p *Player) RevertTurn() {
	p.Tray = p.TrayAtTurnStart.Copy()
}

package rhserv

import (
	"log"

	"github.com/BurntSushi/toml"
)

// Path of the config file.
const configPath = "./rummyhub_conf.toml"
const exitFailure = 1

// Config struct contains the configuration.
type Config struct {
	ExpiryHours int
	Database    databaseConfig
}

type databaseConfig struct {
	Address  string
	Password string
	ID       int
}

// NewConfig reads the Config.
func NewConfig() *Config {
	c := new(Config)
	if _, err := toml.DecodeFile(configPath, c); err != nil {
		log.Fatal("invalid rhserv.conf", err)
	}

	return c
}

package rhserv

import (
	"math/rand"
)

// The number of tiles in the complete set is 13 per suit by four suits by
// two sets plus two wildcards.
const (
	TilesMax = 13*4*2 + 2
)

// Bag represents a single game played by Players in a Room.
type Bag struct {
	Tiles []Tile
}

// NewBag creates a Bag of Tiles.
func NewBag() *Bag {
	b := new(Bag)

	// Put all the tiles in the bag.
	b.Tiles = make([]Tile, TilesMax)

	for i := range b.Tiles {
		b.Tiles[i] = Tile(i + 1)
	}

	return b
}

// DrawTile takes a tile from the Bag at random.
func (b *Bag) DrawTile() Tile {
	// Check if the bag is empty.
	if len(b.Tiles) == 0 {
		return Tile(0)
	}

	// Pick a random tile.
	idx := rand.Intn(len(b.Tiles))
	tile := b.Tiles[idx]

	// Remove it from the bag.
	b.Tiles = append(b.Tiles[:idx], b.Tiles[idx+1:]...)

	return Tile(tile)
}

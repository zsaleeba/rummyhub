package rhserv

import (
	"fmt"
	"sort"
)

// RummySet is a set of tiles groups by matching subsets based on the
// rummy rules.
type RummySet []RummyString

// RummySetFromTileArea analyses a TileArea and produces a RummySet from it.
func RummySetFromTileArea(ta *TileArea) RummySet {
	rs := make(RummySet, 0)
	for row := range ta.Tiles {
		// Keep track of how we're building a string
		inString := false
		startCol := 0
		endCol := 0
		stringEnded := false

		// Run along a row, constructing strings as we go.
		for col := range ta.Tiles[row] {
			t := ta.Tiles[row][col]
			if !inString {
				// Not in a string.
				if t != 0 {
					inString = true
					startCol = col
				}
			} else {
				// In a string.
				if t == 0 {
					// Finished it on the previous column.
					stringEnded = true
					endCol = col
				} else {
					// The string is continuing. But is it the last column?
					if col+1 == len(ta.Tiles[row]) {
						stringEnded = true
						endCol = col + 1
					}
				}
			}

			// If we've ended a string add it to the set.
			if stringEnded {
				rStr := NewRummyString(startCol, row, ta.Tiles[row][startCol:endCol])
				rs = append(rs, *rStr)

				inString = false
				stringEnded = false
			}
		}
	}

	return rs
}

// Equivalent compares two RummySets for equivalence. It ignores the positions of
// the strings on the TileArea and compares the existence of similar valued strings.
func (rs RummySet) Equivalent(rs2 RummySet) bool {
	s1 := rs.Canonicalise()
	s2 := rs2.Canonicalise()
	return s1.Equals(s2)
}

// Canonicalise sorts a RummySet in a canonical order. This ensures that two sorted RummySets
// will be equivalent.
func (rs RummySet) Canonicalise() RummySet {
	rsp := make(RummySet, len(rs))

	for i, v := range rs {
		rsp[i] = v
		rsp[i].Canonicalise()
	}

	sort.Sort(rsp)

	return rsp
}

// GetStatus gets the validity and modified status of a RummySet.
func (rs RummySet) GetStatus(orig *RummySet) (bool, bool) {
	crs := rs.Canonicalise()
	cOrig := orig.Canonicalise()

	// fmt.Println("GetStatus crs")
	// crs.Print()
	// fmt.Println("GetStatus cOrig")
	// cOrig.Print()
	// fmt.Println()

	return crs.IsValid(), !crs.Equals(cOrig)
}

// Equals compares two RummySets. If they contain the same RummyStrings in the
// same order it'll return true. It ignores the placement of the RummyStrings on
// the TileArea.
func (rs RummySet) Equals(rs2 RummySet) bool {
	if len(rs) != len(rs2) {
		return false
	}

	for i := range rs {
		if !rs[i].Equals(&rs2[i]) {
			return false
		}
	}

	return true
}

// IsValid checks that all the RummyStrings in a set are valid.
func (rs RummySet) IsValid() bool {
	for _, t := range rs {
		if t.Type == RummyStringTypeInvalid {
			return false
		}
	}

	return true
}

// Print displays a RummySet for debug purposes.
func (rs RummySet) Print() {
	for _, t := range rs {
		t.Print()
		fmt.Print(" ")
	}
}

// Sorting interface for RummySet.
func (rs RummySet) Len() int           { return len(rs) }
func (rs RummySet) Swap(i, j int)      { rs[i], rs[j] = rs[j], rs[i] }
func (rs RummySet) Less(i, j int) bool { return rs[i].StringLessThan(&rs[j]) }

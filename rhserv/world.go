package rhserv

import "log"

// World is the top-level object for the game. It contains all the
// gameplay-related logic for all the Rooms, Games, Players, etc.
type World struct {
	players map[uint64]*Player
	games   map[uint64]*Game
	Db      *Db
}

// NewWorld creates a World.
func NewWorld(config *Config) *World {
	w := new(World)
	w.players = make(map[uint64]*Player)
	w.games = make(map[uint64]*Game)
	w.Db = NewDb(config)

	return w
}

// PutPlayer adds a Player to the World.
func (w *World) PutPlayer(p *Player) {
	w.players[p.ID] = p

	if err := w.Db.PutPlayer(p); err != nil {
		log.Println("can't put player", p.ID, ":", err)
	}
}

// RemovePlayer removes a Player from the World.
func (w *World) RemovePlayer(p *Player) {
	delete(w.players, p.ID)

	if err := w.Db.RemovePlayer(p); err != nil {
		log.Println("can't remove player", p.ID, ":", err)
	}
}

// GetPlayer gets a Player based on their id.
func (w *World) GetPlayer(id uint64) *Player {
	// Do we have them cached?
	if val, ok := w.players[id]; ok {
		return &*val
	}

	// Are they in the database?
	p, err := w.Db.GetPlayer(id)
	if err != nil {
		log.Println("can't get player", id, ":", err)
		return nil
	}

	// Cache them.
	w.players[p.ID] = p

	return nil
}

// PutGame adds a Game to the World.
func (w *World) PutGame(g *Game) {
	w.games[g.ID] = g

	if err := w.Db.PutGame(g); err != nil {
		log.Println("can't put game", g.ID, ":", err)
	}
}

// PutGameAndPlayers stores the game and the players.
func (w *World) PutGameAndPlayers(g *Game) {
	w.PutGame(g)

	players := g.GetPlayers()
	for _, p := range players {
		w.PutPlayer(p)
	}
}

// RemoveGame removes a Game from the World.
func (w *World) RemoveGame(g *Game) {
	delete(w.games, g.ID)
}

// GetGame gets a Game based on their id.
func (w *World) GetGame(id uint64) *Game {
	// Do we have it cached?
	if val, ok := w.games[id]; ok {
		return &*val
	}

	// Try the database.
	g, err := w.Db.GetGame(id)
	if err != nil {
		log.Println("can't get game", id, ":", err)
		return nil
	}

	// Cache it.
	w.games[g.ID] = g

	return g
}

// GetGameIDByGameCode finds the game with a given game code.
func (w *World) GetGameIDByGameCode(gameCode uint) uint64 {
	return w.Db.GetGameIDByGameCode(gameCode)
}

// NewUniqueGame creates a Game with a unique gameCode.
func (w *World) NewUniqueGame() *Game {
	g := NewGame()
	for w.Db.GetGameIDByGameCode(g.GameCode) != 0 {
		g = NewGame()
	}

	// Add it to the memory cache.
	w.PutGame(g)

	// Add it to the database.
	if err := w.Db.PutGame(g); err != nil {
		log.Fatal("can't write Game to Db", err)
	}

	if err := w.Db.PutGameIDByGameCode(g.GameCode, g.ID); err != nil {
		log.Fatal("can't write GameCode mapping to Db", err)
	}

	return g
}

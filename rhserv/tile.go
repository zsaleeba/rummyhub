package rhserv

import "fmt"

// Tile type.
type Tile int

// Suit Type.
type Suit uint8

// Values that a Suit can take.
const (
	SuitNone Suit = iota
	SuitBlack
	SuitRed
	SuitGreen
	SuitBlue
	SuitBlack2
	SuitRed2
	SuitGreen2
	SuitBlue2
	SuitWild
)

type TileComparison int

const (
	TileLess TileComparison = iota
	TileEqual
	TileGreater
)

// GetVisibleAttrs gets the Suit and face value of a Tile.
func (t Tile) GetVisibleAttrs() (Suit, int) {
	suit, faceValue := t.GetHiddenAttrs()
	switch suit {
	case SuitBlack2:
		suit = SuitBlack
	case SuitRed2:
		suit = SuitRed
	case SuitGreen2:
		suit = SuitGreen
	case SuitBlue2:
		suit = SuitBlue
	}

	return suit, faceValue
}

// GetHiddenAttrs gets the hidden Suit of a Tile and its face value.
func (t Tile) GetHiddenAttrs() (Suit, int) {
	if t == 0 {
		return SuitNone, 0
	}

	suit := (t - 1) / 13
	faceValue := int((t-1)%13) + 1

	return Suit(suit + 1), faceValue
}

// NewTile makes a Tile with a given Suit and face value.
func NewTile(s Suit, val uint8) Tile {
	if s == SuitNone {
		return 0
	}

	return Tile(uint8(s-1)*13 + uint8(val))
}

// Compare two tiles for sorting order.
func (t Tile) Compare(t2 Tile) TileComparison {
	suit1, val1 := t.GetVisibleAttrs()
	suit2, val2 := t2.GetVisibleAttrs()

	if suit1 < suit2 || val1 < val2 {
		return TileLess
	}

	if suit1 > suit2 || val1 > val2 {
		return TileGreater
	}

	return TileEqual
}

// Print displays a Tile for debug purposes.
func (t Tile) Print() {
	suit, val := t.GetVisibleAttrs()
	var suitArray = [10]string{"non", "blk", "red", "grn", "blu", "bk2", "rd2", "gr2", "bl2", "wld"}
	var suitStr = suitArray[suit]
	fmt.Print(suitStr, val)
}

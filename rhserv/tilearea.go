package rhserv

import "errors"

// TileArea represents a 2d array of Tiles. This is used by both the
// table and the trays.
type TileArea struct {
	Cols  int      `json:"cols"`
	Rows  int      `json:"rows"`
	Tiles [][]Tile `json:"tiles"`
}

// NewTileArea creates a TileArea of a given size.
func NewTileArea(cols int, rows int) *TileArea {

	ta := new(TileArea)
	ta.Cols = cols
	ta.Rows = rows

	ta.Tiles = make([][]Tile, rows)
	for i := range ta.Tiles {
		ta.Tiles[i] = make([]Tile, cols)
	}

	return ta
}

// Clear clears the TileArea to empty.
func (ta *TileArea) Clear() {
	for _, row := range ta.Tiles {
		for i := range row {
			row[i] = 0
		}
	}
}

// Copies a TileArea.
func (ta *TileArea) Copy() *TileArea {
	nta := new(TileArea)
	nta.Cols = ta.Cols
	nta.Rows = ta.Rows

	nta.Tiles = make([][]Tile, nta.Rows)
	for i := range nta.Tiles {
		nta.Tiles[i] = make([]Tile, nta.Cols)
		for j := range nta.Tiles[i] {
			nta.Tiles[i][j] = ta.Tiles[i][j]
		}
	}

	return nta
}

// FindEmptySpace finds a place to put a Tile. It'll find the first
// space after all the currently used slots or if the last slot is
// full it'll put it in the first free slot.
func (ta *TileArea) FindEmptySpace() (int, int, error) {
	// Is there space at the end?
	if ta.Tiles[ta.Rows-1][ta.Cols-1] == 0 {
		// There's space at the end. Find the first space after all
		// the currently used slots.
		for r := ta.Rows - 1; r >= 0; r-- {
			if ta.Tiles[r][ta.Cols-1] != 0 {
				// No space in this row. Use the first space on the next row.
				return 0, r + 1, nil
			}

			// Scan the row from end to start.
			for c := ta.Cols - 2; c >= 0; c-- {
				if ta.Tiles[r][c] != 0 {
					// The slot after this one is the first empty one.
					return c + 1, r, nil
				}
			}
		}

		return 0, 0, nil
	} else {
		// No space at the end. Find the first free slot.
		for r := 0; r < ta.Rows; r++ {
			// Scan the row from start to end.
			for c := 0; c < ta.Cols; c++ {
				if ta.Tiles[r][c] == 0 {
					// This slot is free.
					return c, r, nil
				}
			}
		}
	}

	return 0, 0, errors.New("full")
}

// PutTile places a Tile in a given location in the TileArea.
func (ta *TileArea) PutTile(col int, row int, Tile Tile) error {
	if col < 0 || col > ta.Cols {
		return errors.New("invalid column")
	}

	if row < 0 || row > ta.Rows {
		return errors.New("invalid row")
	}

	if ta.Tiles[row][col] != 0 {
		return errors.New("space already taken")
	}

	ta.Tiles[row][col] = Tile
	return nil
}

// CountTiles counts the number of tiles in the TileArea.
func (ta *TileArea) CountTiles() int {
	total := 0

	for _, row := range ta.Tiles {
		for _, t := range row {
			if t != 0 {
				total++
			}
		}
	}

	return total
}

// LocateTile finds a tile in a TileArea.
func (ta *TileArea) LocateTile(t Tile) (int, int) {
	for row, rowTiles := range ta.Tiles {
		for col, tf := range rowTiles {
			if t == tf {
				return col, row
			}
		}
	}

	return 0, 0
}

// IsEmpty checks if a TileArea is empty of tiles.
func (ta *TileArea) IsEmpty() bool {
	for _, rowTiles := range ta.Tiles {
		for _, t := range rowTiles {
			if t != 0 {
				return false
			}
		}
	}

	return true
}

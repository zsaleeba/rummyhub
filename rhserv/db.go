package rhserv

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
)

// Db provides a database abstraction layer. All database
// calls are done through here.
type Db struct {
	db  *redis.Client
	ctx context.Context
}

// NewDb creates a Db struct.
func NewDb(config *Config) *Db {
	db := new(Db)
	db.db = redis.NewClient(&redis.Options{
		Addr:     config.Database.Address,
		Password: config.Database.Password,
		DB:       config.Database.ID,
	})
	db.ctx = context.Background()

	if _, err := db.db.Ping(db.ctx).Result(); err != nil {
		log.Fatal("can't open redis database", err)
	}

	return db
}

// PutPlayer stores a Player in the database.
func (db *Db) PutPlayer(p *Player) error {
	// Note the time this data was saved.
	p.SavedTime = time.Now()

	// Marshall the player data.
	data, err := json.Marshal(p)
	if err != nil {
		return err
	}

	// Write it to the database.
	key := fmt.Sprintf("player:%d", p.ID)
	return db.db.Set(db.ctx, key, string(data), 0).Err()
}

// RemovePlayer removes a Player from the database.
func (db *Db) RemovePlayer(p *Player) error {
	key := fmt.Sprintf("player:%d", p.ID)
	return db.db.Del(db.ctx, key).Err()
}

// GetPlayer gets a Player based on their id.
func (db *Db) GetPlayer(id uint64) (*Player, error) {
	// Read it from the database.
	key := fmt.Sprintf("player:%d", id)
	data, err := db.db.Get(db.ctx, key).Result()
	if err != nil {
		return nil, err
	}

	// Unmarshal it.
	p := new(Player)
	if err = json.Unmarshal([]byte(data), p); err != nil {
		return nil, err
	}

	// If there's no TrayAtTurnStart, create it.
	if p.TrayAtTurnStart == nil {
		p.TrayAtTurnStart = p.Tray.Copy()
	}

	p.WriteMutex = &sync.Mutex{}

	return p, nil
}

// PutGame stores a Game in the database.
func (db *Db) PutGame(g *Game) error {
	// Note the time this data was saved.
	g.SavedTime = time.Now()

	// Marshal the game data.
	data, err := json.Marshal(g)
	if err != nil {
		return err
	}

	// Write it to the database.
	key := fmt.Sprintf("game:%d", g.ID)
	return db.db.Set(db.ctx, key, string(data), 0).Err()
}

// RemoveGame removes a Game from the database.
func (db *Db) RemoveGame(g *Game) error {
	key := fmt.Sprintf("game:%d", g.ID)
	return db.db.Del(db.ctx, key).Err()
}

// GetGame gets a Game based on their id.
func (db *Db) GetGame(id uint64) (*Game, error) {
	// Read it from the database.
	key := fmt.Sprintf("game:%d", id)
	data, err := db.db.Get(db.ctx, key).Result()
	if err != nil {
		return nil, err
	}

	// Unmarshal it.
	g := new(Game)
	if err = json.Unmarshal([]byte(data), g); err != nil {
		return nil, err
	}

	// If there's no TableAtTurnStart, create it.
	if g.TableAtTurnStart == nil {
		g.TableAtTurnStart = g.Table.Copy()
	}

	// Restore the players.
	players := make(map[uint64]*Player)
	for _, pid := range g.PlayerIDs {
		p, err := db.GetPlayer(pid)
		if err != nil {
			return nil, err
		}

		players[p.ID] = p
	}

	g.SetPlayers(players)

	return g, nil
}

// PutGameIDByGameCode stores a mapping from a game code to a game id.
func (db *Db) PutGameIDByGameCode(gameCode uint, gameID uint64) error {
	// Write it to the database.
	key := fmt.Sprintf("gameidbygamecode:%d", gameCode)
	value := strconv.FormatUint(gameID, 10)
	return db.db.Set(db.ctx, key, value, 0).Err()
}

// RemoveGameIDByGameCode removes a GameIDByGameCode mapping from the database.
func (db *Db) RemoveGameIDByGameCode(g *Game) error {
	key := fmt.Sprintf("gameidbygamecode:%d", g.ID)
	return db.db.Del(db.ctx, key).Err()
}

// GetGameIDByGameCode gets a Game ID given a game code.
func (db *Db) GetGameIDByGameCode(gameCode uint) uint64 {
	// Read it from the database.
	key := fmt.Sprintf("gameidbygamecode:%d", gameCode)
	data, err := db.db.Get(db.ctx, key).Result()
	if err != nil {
		log.Println("can't get game code", gameCode, ":", err)
		return 0
	}

	v, err := strconv.ParseUint(string(data), 10, 64)
	if err != nil {
		log.Println("invalid data in game code", gameCode, ":", err)
		return 0
	}

	return v
}

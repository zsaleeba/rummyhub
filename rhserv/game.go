package rhserv

import (
	"errors"
	"log"
	"math/rand"
	"sync"
	"time"
)

// The size of the tile area on the table.
const (
	TableTileCols = 25
	TableTileRows = 8
	DealTiles     = 14
)

// Game represents a single game played by players in a Room.
type Game struct {
	ID                   uint64
	SavedTime            time.Time
	GameCode             uint
	PlayerIDs            []uint64
	players              map[uint64]*Player
	playerMutex          sync.Mutex
	InProgress           bool      // Whether a game is currently occurring.
	PlayersTurn          uint64    // Which player id's turn it is.
	TableModified        bool      // Whether the table has been modified since the start of the turn.
	TableValid           bool      // Whether the table is in a valid state.
	GameWon              bool      // Whether the game has been won by someone.
	GameWonBy            uint64    // If the game was won, who it was won by.
	Table                *TileArea // The table state now.
	TableAtTurnStart     *TileArea // The table state when we started the turn.
	AddedToTableThisTurn []Tile    // The tiles which were added to the table this turn.
	Bag                  *Bag      // The tile bag contents.
	// scores   map[uint64]int
}

// NewGame creates a Game.
func NewGame() *Game {
	g := new(Game)
	g.GameCode = uint(rand.Intn(10000))
	g.ID = MakeRandomID()
	g.Table = NewTileArea(TableTileCols, TableTileRows)
	g.TableAtTurnStart = NewTileArea(TableTileCols, TableTileRows)
	g.Bag = NewBag()
	g.players = make(map[uint64]*Player)
	g.SavedTime = time.Now()
	g.InProgress = false
	g.PlayersTurn = 0
	g.TableModified = false
	g.TableValid = true
	g.GameWon = false
	g.GameWonBy = 0

	return g
}

// StartGame clears the entire game state in preparation for a new game.
func (g *Game) StartGame() error {
	g.Table.Clear()
	g.TableAtTurnStart.Clear()

	players := g.GetPlayers()

	if len(players) < 2 {
		return errors.New("not enough players to start the game")
	}

	for _, p := range g.GetPlayers() {
		p.Tray.Clear()
		p.TrayAtTurnStart.Clear()
		p.Playing = true
		p.Observing = false
	}

	g.Bag = NewBag()

	g.Deal()

	g.InProgress = true
	g.TableModified = false
	g.TableValid = true
	g.GameWon = false
	g.GameWonBy = 0

	// Pick a random person to start.
	g.PlayersTurn = g.PlayerIDs[rand.Intn(len(g.PlayerIDs))]

	return nil
}

// Deal deals tiles to each of the players.
func (g *Game) Deal() {
	for _, p := range g.GetPlayers() {
		for i := 0; i < DealTiles; i++ {
			c, r, err := p.Tray.FindEmptySpace()
			if err != nil {
				log.Println("deal error:", err)
			}

			p.Tray.Tiles[r][c] = g.Bag.DrawTile()
		}
	}
}

// AddPlayer adds a Player to the Game.
func (g *Game) AddPlayer(p *Player) {
	g.playerMutex.Lock()
	g.PlayerIDs = append(g.PlayerIDs, p.ID)
	g.players[p.ID] = p
	g.playerMutex.Unlock()
}

// RemovePlayer removes a Player from the Game.
func (g *Game) RemovePlayer(p *Player) {
	g.playerMutex.Lock()
	delete(g.players, p.ID)

	// Remove them from the array of player ids.
	for i, pid := range g.PlayerIDs {
		if pid == p.ID {
			g.PlayerIDs = append(g.PlayerIDs[:i], g.PlayerIDs[i+1:]...)
			g.playerMutex.Unlock()
			return
		}
	}

	g.playerMutex.Unlock()
}

// GetPlayerByName gets the player in a game with the given name.
func (g *Game) GetPlayerByName(playerName string) *Player {
	g.playerMutex.Lock()
	for _, p := range g.players {
		if p.Name == playerName {
			g.playerMutex.Unlock()
			return p
		}
	}

	g.playerMutex.Unlock()
	return nil
}

// GetPlayerByID gets a player from this game given their ID.
func (g *Game) GetPlayerByID(id uint64) *Player {
	g.playerMutex.Lock()
	p := g.players[id]
	g.playerMutex.Unlock()

	return p
}

// GetPlayers gets a slice of all the players in this game, in their assigned order.
func (g *Game) GetPlayers() []*Player {
	g.playerMutex.Lock()
	players := make([]*Player, len(g.PlayerIDs))
	for i, pid := range g.PlayerIDs {
		players[i] = g.players[pid]
	}

	g.playerMutex.Unlock()

	return players
}

// SetPlayers set the map of players so that it corresponds to the player ids. This is
// done after restoring from the database.
func (g *Game) SetPlayers(players map[uint64]*Player) {
	g.playerMutex.Lock()
	g.players = players
	g.playerMutex.Unlock()
}

// GetBag returns the Bag.
func (g *Game) GetBag() *Bag {
	return g.Bag
}

// Check whether the table has been modified, whether it's valid and whether the
// game has been won.
func (g *Game) UpdateStatus() {
	rs := RummySetFromTileArea(g.Table)
	origRs := RummySetFromTileArea(g.TableAtTurnStart)
	g.TableValid, g.TableModified = rs.GetStatus(&origRs)

	//g.InProgress = false

	// Has anyone won the game?
	g.GameWon = false
	g.GameWonBy = 0

	if g.InProgress {
		players := g.GetPlayers()
		for _, p := range players {
			if p.Tray.CountTiles() == 0 {
				g.GameWon = true
				g.GameWonBy = p.ID
			}
		}
	}
}

// StartOfTurn is called at the start of each turn.
func (g *Game) StartOfTurn() {
	g.TableAtTurnStart = g.Table.Copy()
	g.AddedToTableThisTurn = []Tile{}
}

// TileWasFromTable checks if a tile was on the table at the start of the turn.
func (g *Game) TileWasFromTable(t Tile) bool {
	for _, row := range g.TableAtTurnStart.Tiles {
		for _, foundTile := range row {
			if foundTile == t {
				return true
			}
		}
	}

	return false
}

// TileAreaCoord represents a tile and where it's located on the Table.
type TileAreaCoord struct {
	T   Tile
	Col int
	Row int
}

// RevertTurn reverts a turn's changes.
func (g *Game) RevertTurn() {
	g.Table = g.TableAtTurnStart.Copy()

	// removedTiles := []TileAreaCoord{}
	// for _, t := range g.AddedToTableThisTurn {
	// 	col, row := g.Table.LocateTile(t)
	// 	tileInfo := TileAreaCoord{t, col, row}
	// 	removedTiles = append(removedTiles, tileInfo)
	// }

	// g.AddedToTableThisTurn = []Tile{}

	// return removedTiles
}

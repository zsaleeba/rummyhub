module gitlab.com/zsaleeba/rummyhub

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/go-redis/redis/v8 v8.0.0-beta.6
	golang.org/x/net v0.0.0-20200625001655-4c5254603344
)
